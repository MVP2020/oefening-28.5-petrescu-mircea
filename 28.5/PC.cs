﻿using System.Collections.Generic;

namespace _28._5
{
    class PC
    {


        public List<Printer> Printers { get; set; }

        public PC()
        {
            Printers = new List<Printer>();
        }

        public bool PrintAf()
        {
            foreach (Printer p in Printers)
            {
                if (p.Busy == false)
                {
                    p.Busy = true;
                    return true;
                }
            }
            return false;
        }

        public void AddPrinter(Printer p)
        {
            Printers.Add(p);
        }

    }
}
