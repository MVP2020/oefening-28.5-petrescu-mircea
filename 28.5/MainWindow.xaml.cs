﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace _28._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            InitializeComponent();

        }

        PC pc = new PC();
        Printer p1 = new Printer("Printer 1");
        Printer p2 = new Printer("Printer 2");
        Printer p3 = new Printer("Printer 3");
        Printer p4 = new Printer("Printer 4");

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            if (pc.PrintAf())
            {
                Refreshen();
            }
            else
            {
                MessageBox.Show("Er is geen printer beschikbaar.");
            }
        }

        private void btnResetPrinter1_Click(object sender, RoutedEventArgs e)
        {
            p1.Reset();
            Refreshen();
        }

        private void btnResetPrinter2_Click(object sender, RoutedEventArgs e)
        {
            p2.Reset();
            Refreshen();
        }

        private void btnResetPrinter3_Click(object sender, RoutedEventArgs e)
        {
            p3.Reset();
            Refreshen();
        }

        private void btnResetPrinter4_Click(object sender, RoutedEventArgs e)
        {
            p4.Reset();
            Refreshen();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            pc.AddPrinter(p1);
            pc.AddPrinter(p2);
            pc.AddPrinter(p3);
            pc.AddPrinter(p4);
            Refreshen();
        }

        private void Refreshen()
        {
            StelButtonIn(p1, imgbox1, lbprinter1);
            StelButtonIn(p2, imgbox2, lbprinter2);
            StelButtonIn(p3, imgbox3, lbprinter3);
            StelButtonIn(p4, imgbox4, lbprinter4);
        }

        private void StelButtonIn(Printer p, Image i, Label t)
        {
            BitmapImage bmp = null;
            t.Content = p.ToString();
            if (p.Busy)
            {
                bmp = new BitmapImage(new Uri(@"Images\printer_busy.gif", UriKind.Relative));
            }
            else
            {
                bmp = new BitmapImage(new Uri(@"Images\printer_notbusy.jpg", UriKind.Relative));
            }
            i.Source = bmp;
        }
    }
}
