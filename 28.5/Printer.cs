﻿namespace _28._5
{
    class Printer
    {

        public bool Busy { get; set; }
        public string Naam { get; set; }

        public Printer(string naam)
        {
            this.Naam = naam;
        }
        public void Reset()
        {
            Busy = false;
        }
        public override string ToString()
        {
            if (Busy)
            {
                return Naam + "is printing a document";
            }
            else
            {
                return Naam + "is ready to print a document";
            }
        }
    }
}
